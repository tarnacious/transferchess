-module(server).
-export([run/0]).

run() ->
    {ok, Context} = erlzmq:context(),
    {ok, Requester} = erlzmq:socket(Context, req),
    ok = erlzmq:bind(Requester,"tcp://*:5555"),

    listen(Requester),

    ok = erlzmq:close(Requester),
    ok = erlzmq:term(Context).

listen(Requester) ->
    receive 
        {send} ->
             ok = erlzmq:send(Requester, <<"Hello">>),
             {ok, Reply} = erlzmq:recv(Requester),
             io:format("Received ~s~n", [Reply]),
             listen(Requester);
      	_ ->
	     io:format("Unknown message"),
	     listen(Requester)
    end.

	    

