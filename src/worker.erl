-module(worker).
-export([run/1]).

run(Name) ->
    {ok, Context} = erlzmq:context(),
    {ok, Responder} = erlzmq:socket(Context, rep),
    ok = erlzmq:connect(Responder, "tcp://localhost:5555"),

    loop(Responder, Name),

    ok = erlzmq:close(Responder),
    ok = erlzmq:term(Context).

loop(Responder, Name) ->
    {ok, Msg} = erlzmq:recv(Responder),
    io:format("~s received: ~s~n", [Name, binary_to_list(Msg)]),

    %% Do some 'work'
    timer:sleep(10000),

    %% Send reply back to client
    Message = "reply: " ++ Name,
    ok = erlzmq:send(Responder, list_to_binary(Message)),
    io:format("Responded ~s~n", [Message]),
    loop(Responder, Name).
