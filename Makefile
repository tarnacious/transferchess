ERL ?= erl
APP := letters

.PHONY: deps

all: deps
	@./rebar compile

deps:
	@./rebar get-deps

clean:
	@./rebar clean

distclean: clean
	@./rebar delete-deps

docs:
	@erl -noshell -run edoc_run application '$(APP)' '"."' '[]'

start: all
	exec erl -pa $(PWD)/ebin $(PWD)/deps/*/ebin -s transferchess

console: 
	exec erl -pa $(PWD)/ebin $(PWD)/deps/*/ebin 
